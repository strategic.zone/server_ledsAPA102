#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Created by Stinca
# v => 2
# last modification: 20180522
# import apa102
from scipy import signal
import numpy
from time import sleep, time
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import logging

debug = True
settings = json.load(open("settings/settings.json"))

# Initialize the library and the strip
# strip = apa102.APA102(num_led=50, global_brightness=255, mosi = 10, sclk = 11, order='rgb')
# Turn off all pixels (sometimes a few light up when the strip gets power)
# strip.clear_strip()

class S(BaseHTTPRequestHandler):
	def _set_response(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()

	def do_GET(self):
		print ("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
		self._set_response()
		self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
		myLed = int((self.path)[1:])
		if myLed >=0 and myLed <= 50:
			showLogs (int(myLed))
			fadeLed(myLed)
		else:
			showLogs ("Led error")
def showLogs(msg):
	if debug == True:
		# logging.info(msg)
		print (msg)


def fadeLed (myLed):
	settings = json.load(open("settings/settings.json"))
	timeout = settings["param"]["timeout"]
	fade_speed = settings["param"]["fade_speed"]
	max_brightness = settings["param"]["max_brightness"]
	gaussian = signal.gaussian(settings["param"]["gaussian_size"], settings["param"]["gaussian_sigma"], sym=False)
	brightness =  (numpy.array(gaussian) * max_brightness).astype(int)
	led_color = int(settings["leds"]["led_"+str(myLed)]["color"], 16)
	showLogs (led_color)
	# strip.set_pixel_rgb(myLed, led_color)

	for sig in range (1, (int(len(gaussian)/2+1))):
		# strip.global_brightness = brightness[sig]
		# showLogs (strip.global_brightness)
		showLogs (brightness[sig])
		# strip.set_pixel_rgb(myLed, led_color)
		# strip.show()
		sleep (fade_speed)

	#timeout
	sleep (timeout)

	for sig in range (int(len(gaussian)/2), int(len(gaussian))):
		# strip.global_brightness = brightness[sig]
		# showLogs (strip.global_brightness)
		showLogs (brightness[sig])
		# strip.set_pixel_rgb(myLed, led_color)
		# strip.show()
		sleep (fade_speed)
	# strip.global_brightness = brightness[sig]
	# strip.clear_strip()

def run(server_class=HTTPServer, handler_class=S, port=8080):
	server_address = ('', port)
	httpd = server_class(server_address, handler_class)
	showLogs ('Starting httpd...\n')
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		pass
	httpd.server_close()
	showLogs ('Stopping httpd...\n')
	# strip.clear_strip()
	# strip.cleanup()

if __name__ == '__main__':
	run()
