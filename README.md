# README
## Description

## Dependencies

This project depends on `python 3` : `apt install python3-dev python3-rpi.gpio` 
Install modules with `pip3 install -r requirements.txt`
A Raspberry Pi, running an up-to-date version of Raspbian (the library is tested with the 2017-09-07 version of Raspbian Stretch Lite).
If hardware SPI is used: SPI enabled and active (raspi-config, Interfacing Options, SPI, Enable); The SPI must be free and unused.
For software SPI (bit bang mode): Two free GPIO pins
The Adafruit_Python_GPIO library (https://github.com/adafruit/Adafruit_Python_GPIO)
Library from https://github.com/tinue/APA102_Pi/blob/master/apa102.py

### Webserver
nginx: `apt install nginx`
php: `apt install php-zip php-mbstring php-fpm`
hotspot: `apt install hostapd dnsmasq`

## Installation
### Install Project
`git clone git@gitlab.com:strategic.zone/server_ledsAPA102.git /usr/local/bin/led_server`

### hotspot
Copy: `system_config/hostapd.conf /etc/hostapd/`
Start: `systemctl start hostapd`
Link hostapd config for edit with editor: `ln -s /etc/hostapd/hostapd.conf /usr/local/bin/led_server/led_config` and `chown www-data:www-data /etc/hostapd/hostapd.conf`

### wlan0
Copy: `cp system_config/wlan0 /etc/network/interfaces.d/`
reboot
### Dnsmasq
Copy: `cp system_config/dnsmasq.conf /etc/dnsmasq.conf`
Start: `systemctl restart dnsmasq`


## Start Led Server
Copy `system_config/led_server.service` to `/etc/systemd/system/led_server.service`
Reload systemd files `systemctl daemon-reload`
Start service `systemctl start led_server.service`
Enable service `systemctl enable led_server.service`

## Nginx Web Server
Copy nginx virtual website: `cp system_config/nginx_webserver.conf /etc/nginx/sites-available`
Remove default website: `rm -rf /etc/nginx/sites-enabled/default`
Enable new config: `ln -s /etc/nginx/sites-available/nginx_webserver.conf /etc/nginx/sites-enabled`
Restart NGINX: `systemctl restart nginx`
