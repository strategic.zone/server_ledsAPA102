#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Created by Stinca
# v => 1
# last modification: 20180522
import apa102
from scipy import signal
from time import sleep, time
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import logging

debug = True
gaus_size = 9
gaus_sigma = 1
gaussian = signal.gaussian(gaus_size, gaus_sigma).tolist()

# Initialize the library and the strip
strip = apa102.APA102(num_led=50, global_brightness=255, mosi = 10, sclk = 11, order='rgb')
# Turn off all pixels (sometimes a few light up when the strip gets power)
strip.clear_strip()

class S(BaseHTTPRequestHandler):
	def _set_response(self):
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()

	def do_GET(self):
		print ("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
		self._set_response()
		self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
		myLed = int((self.path)[1:])
		if myLed >=0 and myLed <= 50:
			showLogs (int(myLed))
			blinkLed(myLed)
		else:
			showLogs ("Led error")
def showLogs(msg):
	if debug == True:
		logging.info(msg)

def blinkLed (myLed):
	leds = json.load(open("led_config/leds.json"))
	timeout = time() + leds["param"]["blink_time"]
	speed_brightness = leds["param"]["speed_brightness"]
	speed_blink = leds["param"]["speed_blink"]
	max_brightness = leds["param"]["max_brightness"]
	gaussian = signal.gaussian(gaus_size, gaus_sigma).tolist()
	while True:
		for item in gaussian:
			strip.global_brightness = int((gaussian[0])*max_brightness)			
			showLogs (strip.global_brightness)
			led_color = int(leds["color"][str(myLed)], 16)
			showLogs (leds["color"][str(myLed)])
			strip.set_pixel_rgb(myLed, led_color)
			strip.show()
			sleep (speed_brightness)
			gaussian += [gaussian.pop(0)]
		sleep (speed_blink)
		if time() > timeout:
			break

def run(server_class=HTTPServer, handler_class=S, port=8080):
	server_address = ('', port)
	httpd = server_class(server_address, handler_class)
	showLogs ('Starting httpd...\n')
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		pass
	httpd.server_close()
	showLogs ('Stopping httpd...\n')
	strip.clear_strip()
	strip.cleanup()

if __name__ == '__main__':
	run()
